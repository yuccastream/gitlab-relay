ifeq ($(IMAGE),)
IMAGE = registry.gitlab.com/yuccastream/gitlab-relay
endif

ifeq ($(VERSION),)
VERSION = latest
endif

.DEFAULT_GOAL := build

.PHONY: build
build:
	@echo "==> Building local..."
	@go build -o gitlab-relay *.go

.PHONY: docker
docker:
	@echo "==> Building Docker image..."
	@docker build -t $(IMAGE):$(VERSION) -f ./Dockerfile .

.PHONY: docker-push
docker-push:
	@echo "==> Docker push..."
	@docker push $(IMAGE):$(VERSION)
