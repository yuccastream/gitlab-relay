# GitLab Relay

Ретранслирует комментарии из задач в связанные с ним MR и наоборот.

## Configuration

```
---
listen_address: :8080

gitlab_address: https://gitlab.com
gitlab_token: AAA_BBB

confidential_only: true
mrs_to_issues: true
issues_to_mrs: true
```
