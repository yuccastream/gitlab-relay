package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRelay_AllowReply(t *testing.T) {
	tests := []struct {
		r    *Relay
		conf bool
		want bool
	}{
		{
			r: &Relay{
				c: &Config{
					ConfidentialOnly: false,
				},
			},
			conf: true,
			want: true,
		},
		{
			r: &Relay{
				c: &Config{
					ConfidentialOnly: false,
				},
			},
			conf: false,
			want: true,
		},
		{
			r: &Relay{
				c: &Config{
					ConfidentialOnly: true,
				},
			},
			conf: true,
			want: true,
		},
		{
			r: &Relay{
				c: &Config{
					ConfidentialOnly: true,
				},
			},
			conf: false,
			want: false,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.want, test.r.AllowReply(test.conf))
	}
}
