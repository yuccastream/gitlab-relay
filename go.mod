module gitlab.com/yuccastream/gitlab-relay

go 1.13

require (
	github.com/prometheus/client_golang v1.4.1
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.25.0
	gopkg.in/yaml.v2 v2.2.8
)
