FROM golang:1.14.4-buster as build
WORKDIR /go/src/gitlab.com/yuccastream/gitlab-relay/

COPY . ./
RUN make build

FROM debian:10-slim as release
LABEL io.yucca.gitlab-relay.image=true
WORKDIR /opt/gitlab-relay

RUN apt-get update \
    && apt-get install -y ca-certificates \
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean
COPY --from=build /go/src/gitlab.com/yuccastream/gitlab-relay/gitlab-relay /opt/gitlab-relay/gitlab-relay

EXPOSE 8080
ENTRYPOINT [ "/opt/gitlab-relay/gitlab-relay" ]
CMD [ "-h" ]
