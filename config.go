package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	ConfidentialOnly      bool   `yaml:"confidential_only"`
	GitLabAddress         string `yaml:"gitlab_address"`
	GitLabToken           string `yaml:"gitlab_token"`
	ListenAddress         string `yaml:"listen_address"`
	MergeRequestsToIssues bool   `yaml:"mrs_to_issues"`
	IssuesToMergeRequests bool   `yaml:"issues_to_mrs"`
}

func LoadConfig(confFile string) (*Config, error) {
	data, err := ioutil.ReadFile(confFile)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	return config, yaml.Unmarshal(data, config)
}
