package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/xanzy/go-gitlab"
)

type Relay struct {
	gitlabCli  *gitlab.Client
	gitlabUser *gitlab.User
	c          *Config
}

func New(c *Config) (*Relay, error) {
	gitlabCli := gitlab.NewClient(nil, c.GitLabToken)
	err := gitlabCli.SetBaseURL(c.GitLabAddress)
	if err != nil {
		return nil, err
	}
	gitlabUser, _, err := gitlabCli.Users.CurrentUser(nil)
	if err != nil {
		return nil, err
	}
	return &Relay{
		gitlabUser: gitlabUser,
		gitlabCli:  gitlabCli,
		c:          c,
	}, nil
}

func (r *Relay) relayHandler(w http.ResponseWriter, req *http.Request) {
	eventType := gitlab.WebhookEventType(req)
	log.Printf("Receive %s event", eventType)
	payload, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, "Failed to read request body.", http.StatusInternalServerError)
		return
	}
	event, err := gitlab.ParseWebhook(eventType, payload)
	if err != nil {
		http.Error(w, "Failed to parse webhook.", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "Event received. Have a nice day.")
	if err := r.demuxEvent(eventType, event); err != nil {
		log.Println(err)
	}
}

func (r *Relay) AllowReply(confidential bool) bool {
	if !r.c.ConfidentialOnly {
		return true
	}
	return confidential
}

func (r *Relay) relayMergeRequestCommentToIssue(mce *gitlab.MergeCommentEvent) error {
	opts := &gitlab.GetIssuesClosedOnMergeOptions{}
	issues, _, err := r.gitlabCli.MergeRequests.GetIssuesClosedOnMerge(mce.ProjectID, mce.MergeRequest.IID, opts)
	if err != nil {
		return err
	}
	message := fmt.Sprintf("Reply @%s from !%d<br><blockquote>%s</blockquote>", mce.User.Username, mce.MergeRequest.IID, mce.ObjectAttributes.Note)
	for _, issue := range issues {
		if !r.AllowReply(issue.Confidential) {
			continue
		}
		opts := &gitlab.CreateIssueNoteOptions{
			Body: gitlab.String(message),
		}
		_, _, err := r.gitlabCli.Notes.CreateIssueNote(issue.ProjectID, issue.IID, opts)
		if err != nil {
			log.Println(err)
		}
	}
	return nil
}

func (r *Relay) relayIssueCommentToMergeRequest(ice *gitlab.IssueCommentEvent) error {
	if !r.AllowReply(ice.Issue.Confidential) {
		return nil
	}
	opts := &gitlab.ListMergeRequestsClosingIssueOptions{}
	mrs, _, err := r.gitlabCli.Issues.ListMergeRequestsClosingIssue(ice.ProjectID, ice.Issue.IID, opts)
	if err != nil {
		return err
	}
	message := fmt.Sprintf("Reply @%s from #%d<br><blockquote>%s</blockquote>", ice.User.Username, ice.Issue.IID, ice.ObjectAttributes.Note)
	for _, mr := range mrs {
		opts := &gitlab.CreateMergeRequestNoteOptions{
			Body: gitlab.String(message),
		}
		_, _, err := r.gitlabCli.Notes.CreateMergeRequestNote(mr.ProjectID, mr.IID, opts)
		if err != nil {
			log.Println(err)
		}
	}
	return nil
}

func (r *Relay) demuxEvent(eventType gitlab.EventType, event interface{}) error {
	switch event := event.(type) {
	case *gitlab.MergeCommentEvent:
		if event.User.Username != r.gitlabUser.Username && r.c.MergeRequestsToIssues {
			log.Printf("Relaying from merge request %d to related issues", event.MergeRequest.IID)
			return r.relayMergeRequestCommentToIssue(event)
		}
	case *gitlab.IssueCommentEvent:
		if event.User.Username != r.gitlabUser.Username && r.c.IssuesToMergeRequests {
			log.Printf("Relaying from issue %d to related merge requests", event.Issue.IID)
			return r.relayIssueCommentToMergeRequest(event)
		}
	}
	return nil
}

func (r *Relay) Run() error {
	http.HandleFunc("/relay", r.relayHandler)
	http.Handle("/metrics", promhttp.Handler())
	return http.ListenAndServe(r.c.ListenAddress, nil)
}
