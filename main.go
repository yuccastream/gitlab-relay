package main

import (
	"flag"
	"log"
)

var flagConfig = flag.String("config", "config.yaml", "Configuration file in YAML")

func main() {
	flag.Parse()
	config, err := LoadConfig(*flagConfig)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Listening address: %s", config.ListenAddress)
	relay, err := New(config)
	if err != nil {
		log.Fatal(err)
	}
	err = relay.Run()
	if err != nil {
		log.Fatal(err)
	}
}
